from django.urls import path
from sign import views

urlpatterns = [
    path('',views.login,name='login'),
    path('register/',views.register,name='register'),
]