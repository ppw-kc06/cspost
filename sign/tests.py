from django.contrib.auth.models import User
from django.contrib.auth import SESSION_KEY
from django.test import TestCase, Client
from .models import UserInfo
from .forms import RegisterForm, UserForm

class LogInTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
    def test_login(self):
        response = self.client.post('/login/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)

class TestUserRegistrationView(TestCase):

	def test_registration(self):
		response = self.client.get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_user_form_valid(self):
		form_data = {
				'username' : 'Compfest',
				'password' : 'abcdefgh',
		}
		post_form = UserForm(data=form_data)
		self.assertTrue(post_form.is_valid())

	def test_register_form_valid(self):
		form_data = {
				'username' : 'Compfest',
				'password' : 'abcdefgh',
				'password_repeat' : 'abcdefgh',
		}
		post_form = RegisterForm(data=form_data)
		self.assertTrue(post_form.is_valid())
