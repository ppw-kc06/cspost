from django.test import TestCase
from django.urls import resolve
from post.models import Post
from .views import details,eventdetail
# Create your tests here.
class EventTest(TestCase):
    def test_url_exists(self):
        response = self.client.get('/event/')
        self.assertEqual(response.status_code, 200)
    def test_uses_eventdetail_view(self):
        handler = resolve('/event/')
        self.assertEqual(handler.func, eventdetail)
    # def test_uses_details_view(self):
    #     sample_event = Post.objects.create(
    #         nama_Kegiatan = "Compfest",
    #         Lomba = True,
    #         Organisasi =  True,
    #         Seminar= True,
    #         tanggal = 12/12/12,
    #         lokasi= "Fasilkom",
    #         contact= "testtest",
    #         deskripsi = "asikbanget",
    #         syarat_dan_ketentuan = "nakskeren",
    #     )
        # sample = Post(objects=sample_event)
        # post_form = PostForm(data=form_data)
        # self.assertTrue(post_form.is_valid())
        # handler = resolve('/event/details/1')
        # self.assertEqual(handler.func, details)
        # self.assertTrue(Post.objects.filter(id='1').exists())