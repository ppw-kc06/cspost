from django.shortcuts import render
from post.models import Post

# Create your views here.
def eventdetail(request):
    return render(request,'pages/event.html',{})

def details(request, event_id):
	# context = {}
	catch = Post.objects.filter(id=event_id)[0]
	context = {
		'catch' : catch
	}
	html = 'pages/event.html'
	return render(request, html, context)