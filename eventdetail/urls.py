from django.urls import path
from . import views

app_name = 'post'
urlpatterns = [
    path('',views.eventdetail,name='index'),
    path('details/<int:event_id>', views.details, name ='details'),
]