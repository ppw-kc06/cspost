from django import forms
from .models import Post
##Create your forms here
class PostForm(forms.ModelForm):
    """Form definition for MODELNAME."""

    class Meta:
        """Meta definition for MODELNAMEform."""

        model = Post
        fields = '__all__'
        widgets = {
          'contact': forms.Textarea(attrs={'rows':4, 'cols':25}),
          'deskripsi': forms.Textarea(attrs={'rows':4, 'cols':25}),
          'syarat_dan_ketentuan': forms.Textarea(attrs={'rows':4, 'cols':25}),
        }