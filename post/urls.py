from django.urls import path
from . import views


urlpatterns = [
    path('createpost/',views.post,name='postevent'),
    path('',views.home,name='index'),
]