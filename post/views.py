from django.shortcuts import render,redirect,reverse
from .models import Post
from .forms import PostForm
from django.db.models import Q

# Create your views here.
def home(request):
    object_list = Post.objects.all()
    context = {'table_list' : object_list}
    query = request.GET.get('q')
    if(query):
        object_list = Post.objects.filter(
            Q(nama_Kegiatan__icontains=query) | Q(deskripsi__icontains=query)
        )
        context['table_list'] = object_list
    filterkategori = request.GET.get('filter')
    if(filterkategori):
        if(filterkategori=='organisasi'):
           context['table_list'] = Post.objects.filter(Organisasi=True)
        elif(filterkategori=='seminar'):
            context['table_list'] = Post.objects.filter(Seminar = True)
        elif(filterkategori=='lomba'):
            context['table_list']  = Post.objects.filter(Lomba = True)
    return render(request,'pages/index.html', context)

def post(request):
    posts = Post.objects.all(); 
    form = PostForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            u = form.save()
            return redirect(reverse('postevent'))
    context={
        'form' : form
    }
    return render(request,'pages/postevent.html', context)
