from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from sign import views

urlpatterns = [
	path('',include('home.urls'), name= 'home'),
    path('admin/', admin.site.urls),
    path('login/',include('sign.urls'), name= 'sign'),
    path('',include('home.urls')),
    path('post/',include('post.urls')),
    path('event/',include('eventdetail.urls', namespace='event'))
]
