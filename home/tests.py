from django.test import TestCase
from django.test import TestCase
from django.urls import resolve
from .views import home
# Create your tests here.
class HomeTest(TestCase):
    def test_url_exists(self):
        response = self.client.get('/post/')
        self.assertEqual(response.status_code, 200)
    def test_uses_home_view(self):
        handler = resolve('/')
        self.assertEqual(handler.func, home)
    
